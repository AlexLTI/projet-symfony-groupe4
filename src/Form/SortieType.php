<?php

namespace App\Form;

use App\Entity\Sortie;
use App\Entity\Lieu;
use App\Entity\Ville;
use App\Repository\SortieRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class SortieType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class,[
                'label' => 'Nom de la sortie',
                'required' => true,
                'attr' => [
                    'class' => 'bigField',
                ],
            ])
            ->add('dateheuredebut', DateTimeType::class,[
                'label' => 'Date et heure de la sortie',
                'required' => true,
                'attr' => [
                    'class' => 'bigField',
                    'placeholder' => 'dd/mm/yyyy hh:mm',
],
            ])
            ->add('duree', IntegerType::class, [
                'label' => 'Durée:',
                'required' => true,
                'attr' => [
                    'class' => 'bigField',
                    'placeholder' => '90',
                    ],
        ])
            ->add('datelimiteinscription', DateType::class,[
                'label' => 'Date limite d\'inscription:',
                'required' => true,
                'attr' => [
                    'class' => 'bigField',
                    'placeholder' => 'dd/mm/yyyy',
                ],
                ])
            ->add('nbinscriptionsmax',NumberType::class, [
        'label' => 'Nombre de places:',
        'required' => true,
        'attr' => [
            'class' => 'bigField',
        ],
    ])
            ->add('infossortie', TextareaType::class, [
                'label' => 'Description et infos',
                'required' => true,
                'attr' => [
                    'class' => 'bigField',
                ],
            ])

            ->add('lieu', EntityType::class, [
                'label' => 'Lieu:',
                'class' => Lieu::class,
                'choice_label' => 'nom',
                ],
            )
            ->add('latitude', EntityType::class, [
                'mapped' => false,
                'class' => Lieu::class,
                'choice_label' => 'Latitude',
            ],
            )
            ->add('longitude', EntityType::class, [
                'mapped' => false,
                'class' => Lieu::class,
                'choice_label' => 'Longitude',
            ],
            )

            /*->add('ville', EntityType::class, [
                'class'        => Ville::class,
                'choice_label' => 'nom',
                'required' => true,
                'attr' => [
                    'class' => 'bigField',
                ],])

            ->add('ville', EntityType::class, [
               'class' => Ville::class,
               'choice_label' => 'nom',
               ],
           )
          ->add('latitude', EntityType::class, [
               'class' => Lieu::class,
               'choice_label' => 'Latitude',
               ],
           )
           ->add('longitude', EntityType::class, [
               'class' => Lieu::class,
               'choice_label' => 'Longitude',
               ],
           )*/
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sortie::class,
        ]);
    }
}
