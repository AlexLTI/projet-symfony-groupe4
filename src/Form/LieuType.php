<?php

namespace App\Form;

use App\Entity\Ville;
use App\Entity\Lieu;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LieuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ville', EntityType::class, [
                'class'        => Ville::class,
                'choice_label' => 'nom',
                'required' => true,
                'attr' => [
                    'class' => 'bigField',
            ],])

            ->add('nom', TextType::class,[
                'label' => 'Lieu',
                'required' => true,
                'attr' => [
                    'class' => 'bigField',
                ],
            ])
            ->add('rue',TextType::class,[
        'label' => 'Rue',
        'required' => true,
        'attr' => [
            'class' => 'bigField',
        ],
    ])
            ->add('latitude',IntegerType::class,[
        'label' => 'Latitude:',
        'required' => true,
        'attr' => [
            'class' => 'bigField',
        ],
    ])
            ->add('longitude',IntegerType::class,[
        'label' => 'Longitude:',
        'required' => true,
        'attr' => [
            'class' => 'bigField',
        ],
    ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Lieu::class,
        ]);
    }
}
