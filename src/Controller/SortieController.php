<?php

namespace App\Controller;


use App\Entity\Sortie;
use App\Form\SortieType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SortieController extends AbstractController
{
    #[Route('sortie/create', name: 'sortie_create')]
    public function create(Request $request, EntityManagerInterface $entityManager)
    {

        $sortie = new Sortie;
        $sortieForm = $this->createForm(SortieType::class, $sortie);

        $sortieForm->handleRequest($request);

        if ($sortieForm->isSubmitted() && $sortieForm->isValid()) {

            $entityManager->persist($sortie);
            $entityManager->flush();

            $this->addFlash('success', 'Sortie publiée');

            return $this->redirectToRoute('sortie_create', [
                'id' => $sortie->getId(),
        ]);
    }
        return $this->render('sortie/create.html.twig', [
            'sortieForm' => $sortieForm->createView(),
        ]);
    }


    #[Route('sortie/details/{id}', name: 'sortie_details', requirements: [ 'id' => '\d+' ])]
    public function details(int $id, EntityManagerInterface $entityManager) : Response
    {
        $repository = $entityManager->getRepository(Sortie::class);
        $sortie = $repository->find($id);


        return $this->render('sortie/details.html.twig',
            ['sortie' => $sortie]);
    }
}
